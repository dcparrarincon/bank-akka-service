# README #
### Simple Bank Rest API over AKKA actor model and with work pulling pattern ###
* Server:  http server , this is the main class, must be run in order to make the http server available

#### Sample requests:

* [GET] http://localhost:8080/api/account?account_number=acc-c
* [POST] http://localhost:8080/api/transaction?from_account=acc-a&to_account=acc-c&amount=10

* Account: Account actor with its behaviors (Withdraw and Deposit)

* AccountRepository: Actor holding live Accounts over actor system

* Bank: Account and transaction handlers, responsible for properly handling the transactions distributing the incomming transactions over the workers. Is also the Master Actor

* Transfer: Worker actor, responsible for perfoming the transference.

### Preloaded accounts in memory ###

#### Account identifiers
* acc-a
* acc-b
* acc-c
* acc-d
  
## Same bank account
* AccountIdentifier > acc-a
* BankIdentifier > galicia
* Country > ar
* Balance > 100

* AccountIdentifier > acc-b
* BankIdentifier > galicia
* Country > ar
* Balance > 100

## Diffent Bank Account
* AccountIdentifier > acc-c
* BankIdentifier > itau
* Country > ar
* Balance > 100

## Different Country Account
* AccountIdentifier > acc-d
* BankIdentifier > chase
* Country > usa
* Balance > 100
