package com.dcparrarincon


import com.dcparrarincon.Transaction.{International, NationalDifferentBank, NationalSameBank}
import junit.framework.TestCase
import org.junit.Assert._

class TaxTest extends TestCase{




  def testNationalTransactionWithDifferentBank {
    var rate = Transaction.taxRate(NationalDifferentBank)
    assertEquals(0.01,rate, 0.005)

  }

  def testNationalTransactionWithSametBank {
    var rate = Transaction.taxRate(NationalSameBank)
    assertEquals(0.0,rate, 0.001)

  }

  def testInternationalTransactionWith {
    var rate = Transaction.taxRate(International)
    assertEquals(0.05,rate, 0.005)

  }




}



