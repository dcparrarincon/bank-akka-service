/*
* Master actor >> Bank
* Worker actor >> Transaction
*
* Implementation of the  work pulling pattern
*
*
* */

package com.dcparrarincon

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.dcparrarincon.AccountRepository.GetAccount

import scala.collection.mutable
import scala.concurrent.Await
import scala.util.Random

object Transaction {
  def props(accountRepository: ActorRef, bank: ActorRef) = Props(new Transaction(accountRepository, bank))

  trait TransactionWorkerState

  case object Working extends TransactionWorkerState
  case object Idle extends TransactionWorkerState

  trait TransactionType
  case object International extends TransactionType
  case object NationalSameBank extends TransactionType
  case object NationalDifferentBank extends TransactionType

  def taxRate(transactionType: TransactionType):Double={
    transactionType match {
      case NationalDifferentBank => 0.01
      case International => 0.05
      case _ => 0.0
    }
  }

}

class Transaction(accountRepository: ActorRef,
                  bank: ActorRef) extends Actor {

 import Transaction.{TransactionType,International,NationalDifferentBank}


  var fromAccount: ActorRef = _
  var toAccount: ActorRef = _
  var amount: Double = _
  var tax = 0.0
  var id = Random.nextLong()





  import scala.concurrent.duration.DurationDouble



  override def receive: Receive = {

    case Bank.Transfer(fromAccountIdentifier, toAccountIdentifier, amount) =>
      this.amount = amount
      implicit val timeout : Timeout = 3.seconds

      val fromData = Await.result(accountRepository ? AccountRepository.GetAccountInfo(fromAccountIdentifier), 5 seconds).asInstanceOf[AccountInfo]
      val toData = Await.result(accountRepository ? AccountRepository.GetAccountInfo(toAccountIdentifier), 5 seconds).asInstanceOf[AccountInfo]
      var tax =0.0

      var calculateTax:Double={
        if(fromData.country==toData.country){
          if(fromData.bankIdentifier!=toData.bankIdentifier){
            tax=amount*Transaction.taxRate(NationalDifferentBank)
          }
        }else{
          tax=amount*Transaction.taxRate(International)
        }
        tax
      }


      accountRepository ! GetAccount(fromAccountIdentifier)

      context.become({
        case AccountRepository.AccountFound(fromAccount) =>

          this.fromAccount = fromAccount
          accountRepository ! GetAccount(toAccountIdentifier)

          context.become({
            case AccountRepository.AccountFound(toAccount) => this.toAccount = toAccount

              context.unbecome()
              context.unbecome()

              self ! ("PerformTransfer",tax)
            case AccountRepository.AccountNotFound =>
              bank ! Bank.TransferFail(s"target account $toAccountIdentifier does not exists")

          })
        case AccountRepository.AccountNotFound =>
          bank ! Bank.TransferFail(s"source account $fromAccountIdentifier} does not exists")

      })

    case ("PerformTransfer",tax:Double) =>

      fromAccount ! Account.Withdraw(amount)
      fromAccount ! Account.Withdraw(tax)

      context.become({
        case Account.AccountOperationSuccess =>
          toAccount ! Account.Deposit(amount)

          context.become({
            case Account.AccountOperationSuccess =>
              bank ! Bank.TransferSuccessful
              context.unbecome()
            case Account.AccountOperationFail(msg) =>
                    bank ! Bank.TransferFail(msg)
              fromAccount ! Account.Deposit(amount) //reverse the operation
              context.unbecome()
          })
        case Account.AccountOperationFail(msg) =>
          bank ! Bank.TransferFail(msg)
          context.unbecome()
      })

    case u =>
      println(s"unexpected message: $u")
  }
}

object Bank {

  def props(accountRepository: ActorRef) = Props(new Bank(accountRepository))

  case class Transfer(fromAccount: String, toAccount: String, amount: Double)

  case object TransferEnqueued
  case object TransferBegin
  case object TransferSuccessful
  case class TransferFail(reason:String)
  case object AddTransactionWorker
}


class Bank(accountRepository: ActorRef) extends Actor {

  import Bank._
  import Transaction._

  val transactionWorkers = mutable.Map.empty[ActorRef, TransactionWorkerState]
  val pendingTransactions = mutable.Queue.empty[Transfer]

  override def preStart(): Unit = {
    self ! AddTransactionWorker
    self ! AddTransactionWorker
  }

  override def receive: Receive = {

    case AddTransactionWorker =>
      transactionWorkers += (context.actorOf(Transaction.props(accountRepository, self)) -> Idle)


    case transfer : Transfer =>
      pendingTransactions.enqueue(transfer)
      println(transactionWorkers)
      println(pendingTransactions)
      sender ! TransferEnqueued

      transactionWorkers.find (_._2 == Idle) match {
        case Some ((worker, _) ) =>
          val nextTransaction = pendingTransactions.dequeue
          worker ! nextTransaction
          transactionWorkers += worker -> Working
        case None => print("all workers are busy")
      }

    case TransferSuccessful  =>
      println("transfer successful")
      if(pendingTransactions.isEmpty){
        transactionWorkers += sender -> Idle
      } else {
        sender ! pendingTransactions.dequeue()
      }
    case TransferFail(msg) =>
      println("transfer fail")
      if(pendingTransactions.isEmpty){
        transactionWorkers += sender -> Idle
      } else {
        sender ! pendingTransactions.dequeue()
      }
  }


}