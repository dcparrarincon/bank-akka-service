/*
*  Account actor
*  Account operations
*
* */
package com.dcparrarincon

import akka.actor.{Actor, Props}
import spray.json.DefaultJsonProtocol.jsonFormat4
import spray.json.{DefaultJsonProtocol, PrettyPrinter}

object Account {

  def props(accountInfo: AccountInfo) = Props(new Account(accountInfo))

  case class Withdraw(amount: Double)

  case class Deposit(amount: Double)

  case object AccountOperationSuccess

  case class AccountOperationFail(reason: String)

}


final case class AccountInfo(val identifier: String,
                  var balance: Double,
                  bankIdentifier: String = "bank1",
                  country: String = "ar")



class Account(accountInfo: AccountInfo) extends Actor {

  import Account._

  override def receive: Receive = {
    case Withdraw(amount) =>
      if (amount > accountInfo.balance) {
        sender ! AccountOperationFail("insuficient funds")
      } else {
        accountInfo.balance -= amount
        println(s"withdraw($amount) -> ${accountInfo.identifier} : ${accountInfo.balance}")
        sender ! AccountOperationSuccess
      }
    case Deposit(amount) =>
      accountInfo.balance += amount
      println(s"desposit($amount) -> ${accountInfo.identifier} : ${accountInfo.balance}")
      sender ! AccountOperationSuccess
  }
}
