/*
*  Http server
* */

package com.dcparrarincon

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives.{complete, get, path, _}
import akka.stream.ActorMaterializer
import akka.pattern.ask
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scala.io.StdIn
import akka.util.Timeout
import com.dcparrarincon.AccountRepository.GetAccountInfo
import com.dcparrarincon.Bank.Transfer

import scala.concurrent.duration.DurationDouble

import spray.json._


object JsonFormatter extends DefaultJsonProtocol {
  implicit val accountFotmat = jsonFormat4(AccountInfo)
}
object Server extends App {
  import JsonFormatter._


  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher


  val accountRepository = system.actorOf(Props[AccountRepository])

  val bank = system.actorOf(Bank.props(accountRepository))

  accountRepository ! AccountRepository.CreateAccount("acc-a", 100,"galicia","ar")
  accountRepository ! AccountRepository.CreateAccount("acc-b", 100,"galicia","ar")
  accountRepository ! AccountRepository.CreateAccount("acc-c", 100,"itau","ar")
  accountRepository ! AccountRepository.CreateAccount("acc-d", 100,"chase","usa")



  val apiRoutes =
    pathPrefix("api") {
      path("account" ) {
        get {
          parameters('account_number.as[String]) { (account_number) =>
            implicit val timeout : Timeout = 3.seconds
            onSuccess(accountRepository ? AccountRepository.GetAccountInfo(account_number)){ accountRef =>

              accountRef match {
                case accountInfo : AccountInfo =>
                  complete(StatusCodes.OK, accountInfo.toJson)
                case _ =>
                  complete(s"account= ${accountRef}") //TO-DO Parse to json
              }

            }
          }
        }
      } ~
        path("transaction") {
          post {
            parameters('from_account.as[String],'to_account.as[String],'amount.as[Int]) { (fromAccount,toAccount,amount) =>
              implicit val timeout : Timeout = 30.seconds
              onSuccess(bank ? Transfer(fromAccount, toAccount, amount)) { result =>
                result match {
                  case Bank.TransferEnqueued =>
                    complete((StatusCodes.OK, "Transfer enqueued"))
                  case Bank.TransferSuccessful =>
                    complete((StatusCodes.OK, "Success"))
                  case Bank.TransferFail(msg) =>
                    complete((StatusCodes.ExpectationFailed, msg))
                }
              }
            }
          }
        }
    }

  val bindingFuture = Http().bindAndHandle(apiRoutes, "localhost", 8080)

  println("Up :)")

  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())

}