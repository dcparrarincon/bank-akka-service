/*
* Actor holding all the like actor accounts in the actor system
* */

package com.dcparrarincon

import akka.actor.{Actor, ActorRef}

object AccountRepository {

  case class CreateAccount(identifier: String,balance: Double = 0, bankIdentifier:String, country:String)

  case class GetAccount(val identifier: String)

  case class GetAccountInfo(val identifier: String)

  case class AccountFound(account: ActorRef)

  case object AccountNotFound

  case object AccountAlreadyExists

  case object AccountRegistered

}

class AccountRepository extends Actor {

  import AccountRepository._

  var accounts = collection.mutable.Map[String, ActorRef]()
  var accountsInfo = collection.mutable.Map[String, AccountInfo]()

  override def receive: Receive = {
    case GetAccount(identifier) =>
      if (accounts.contains(identifier)) {
        sender ! AccountFound(accounts.get(identifier).get)
      } else {
        sender ! AccountNotFound
      }

    case GetAccountInfo(identifier) =>
      if (accountsInfo.contains(identifier)) {
        sender ! accountsInfo.get(identifier).get
      } else {
        sender ! AccountNotFound
      }

    case CreateAccount(identifier,balance,bankIdentifier,country) =>
      val accountExists = accounts.contains(identifier)
      if (accountExists) {
        sender ! AccountAlreadyExists
      } else {
        val accountInfo = AccountInfo(identifier,balance, bankIdentifier ,country)
        accountsInfo += identifier -> accountInfo
        accounts += identifier -> context.actorOf(Account.props(accountInfo))
        sender ! AccountRegistered
      }

    case unexpectedMessage =>
      println(s"unexcpected message to repository: $unexpectedMessage")
  }
}